import React, { Component } from "react";
// import * as THREE from "three"; did not use
import * as d3 from "d3";
// import TinyQueue from "tinyqueue";
import "../css/Force.css";

export default class Force extends Component {
	componentDidMount() {
		this.draw(this.props);
	}

	render() {
		return <div className='force' />;
	}

	draw = props => {
		d3.json("./poke-node-link.json").then(function(graph) {
			var svg = d3.select("body").append("svg"),
				width = +svg.attr("width"),
				height = +svg.attr("height");

			var color = d3
				.scaleOrdinal()
				.range([
					"#A8A77A",
					"#B6A136",
					"#EE8130",
					"#F95587",
					"#7AC74C",
					"#735797",
					"#6390F0",
					"#96D9D6",
					"#C22E28",
					"#F7D02C",
					"#A6B91A",
					"#A33EA1",
					"#705746",
					"#B7B7CE",
					"#6F35FC",
					"#E2BF65",
					"#A98FF3",
					"#D685AD"
				]);

			var radius = d3.scaleSqrt().range([0, 6]);

			var simulation = d3
				.forceSimulation()
				.force(
					"link",
					d3
						.forceLink()
						.id(function(d) {
							return d.id;
						})
						.distance(function(d) {
							return radius(d.source.size * 5) + radius(d.target.size * 5);
						})
						.strength(function(d) {
							return 0.75;
						})
				)
				.force("charge", d3.forceManyBody().strength(-300))
				.force(
					"collide",
					d3.forceCollide().radius(function(d) {
						return radius(d.size * 10) + 2;
					})
				)
				.force("center", d3.forceCenter(width / 2, height / 2));

			// for zoom
			var group = svg.append("g").attr("class", "group");

			var link = group
				.append("g")
				.attr("class", "links")
				.selectAll("path")
				.data(graph.links)
				.enter()
				.append("svg:path")
				.attr("stroke-width", function(d) {
					return 1;
				})
				.attr("class", function(d) {
					return "link-" + d.id;
				});

			link
				.style("fill", "none")
				.style("stroke", "black")
				.style("stroke-width", ".5px");

			link // not showing, only in views
				.append("text")
				.attr("dy", "1em")
				.attr("text-anchor", "start")
				.text(function(d) {
					return d.dist;
				})
				.style("fill", "black");

			var node = group
				.append("g")
				.attr("class", "nodes")
				.selectAll("g")
				.data(graph.nodes)
				.enter()
				.append("g")
				.attr("class", function(d) {
					return "node-" + d.id;
				})
				.style("transform-origin", "50% 50%")
				.call(
					d3
						.drag()
						.on("start", dragstarted)
						.on("drag", dragged)
						.on("end", dragended)
				);

			node
				.append("circle")
				.attr("r", function(d) {
					return radius(d.size * 5);
				})
				.attr("fill", function(d) {
					return color(d.group);
				});

			node
				.append("text")
				.attr("dy", ".35em")
				.attr("text-anchor", "middle")
				.text(function(d) {
					return d.name;
				});

			simulation.nodes(graph.nodes).on("tick", ticked);

			simulation.force("link").links(graph.links);

			// zoom function
			var zoom_handler = d3.zoom().on("zoom", zoom_actions);
			zoom_handler(svg);

			function zoom_actions() {
				group.attr("transform", d3.event.transform);
			}

			var zoomer = group
				.append("rect")
				.call(zoom_handler)
				.call(zoom_handler.transform, d3.zoomIdentity.scale(0.5, 0.5).translate(600, 600));

			function ticked() {
				link.attr("d", function(d) {
					var dx = d.target.x - d.source.x,
						dy = d.target.y - d.source.y,
						dr = Math.sqrt(dx * dx + dy * dy);
					return "M" + d.source.x + "," + d.source.y + "A" + dr + "," + dr + " 0 0,1 " + d.target.x + "," + d.target.y;
				});

				node.attr("transform", function(d) {
					return "translate(" + d.x + "," + d.y + ")";
				});
			}

			function dragstarted(d) {
				if (!d3.event.active) simulation.alphaTarget(0.3).restart();
				d.fx = d.x;
				d.fy = d.y;
			}

			function dragged(d) {
				d.fx = d3.event.x;
				d.fy = d3.event.y;
			}

			function dragended(d) {
				if (!d3.event.active) simulation.alphaTarget(0);
				d.fx = null;
				d.fy = null;
			}

			// function click() {
			// 	d3.select(this)
			// 		.select("circle")
			// 		.transition()
			// 		.style("stroke", "red");
			// }
			// node.on("click", click);

			// informed search
			// test nodes
			var start = "Gyarados";
			var end = "Butterfree";

			var visitElement = function(element, animate) {
				d3.select(".node-" + element.id)
					.select("circle")
					.transition()
					.duration(250)
					.delay(250 * animate)
					.style("fill", "gray")
					.style("stroke", "gray");
			};

			var visitGoal = function(element, animate) {
				d3.select(".node-" + element.id)
					.select("circle")
					.transition()
					.duration(250)
					.delay(250 * animate)
					.style("fill", "blue")
					.style("stroke", "blue");

				graph.links.forEach(getId => {
					if (element.id === getId.source.id) {
						d3.select("path.link-" + getId.id)
							.transition()
							.duration(250)
							.delay(250 * animate)
							.style("fill", "none")
							.style("stroke", "blue")
							.style("stroke-width", "3px");
					}
				});
			};

			var nodes = [];
			var dataBinding = data => {
				var nodesByName = {};
				graph.nodes.forEach(d => {
					var name = d.name;
					if (!nodesByName[name]) {
						var node = {
							name: name,
							links: [],
							type1: d.type1,
							type2: d.type2
						};
						nodesByName[name] = node;
						nodes.push(node);
					}
				});

				graph.links.forEach(d => {
					nodes.forEach(n => {
						var l = {
							source: d.source,
							target: d.target,
							value: d.dist,
							id: d.id
						};
						if (n.name === d.source.name) {
							n["links"].push(l);
						}
						if (n.name === d.source.type1 && n.name === l.target) {
							n["links"].push(l);
						}
					});
				});
			};
			dataBinding(graph);

			class QElement {
				constructor(element, priority) {
					this.element = element;
					this.priority = priority;
				}
			}
			class PriorityQueue {
				constructor() {
					this.items = [];
				}

				enqueue(element, priority) {
					var qElement = new QElement(element, priority);
					var contain = false;

					for (var i = 0; i < this.items.length; i++) {
						if (this.items[i].priority > qElement.priority) {
							this.items.splice(i, 0, qElement);
							contain = true;
							break;
						}
					}

					if (!contain) {
						this.items.push(qElement);
					}
				}

				dequeue() {
					if (this.isEmpty()) return "Underflow";
					return this.items.shift();
				}

				isEmpty() {
					return this.items.length === 0;
				}
			}

			var dijkstra = (data, start, end) => {
				var pq = new PriorityQueue();
				let prev = {};
				let distances = {};
				let source, goal, goalType;
				let path = [],
					link = [];
				let animate = 0;

				data.forEach(node => {
					if (start === node.name) {
						source = node;
					}
					if (end === node.name) {
						goal = node;
					}
				});
				data.forEach(node => {
					if (goal.type1 === node.name) {
						goalType = node;
					}
				});

				distances[source.name] = 0;
				pq.enqueue(source, 0);
				path.push({ name: source.name, distance: distances[source.name] });
				graph.nodes.forEach(gn => {
					if (gn.name === source.name) {
						visitElement(gn, animate);
					}
				});

				data.forEach(node => {
					if (node.name !== source.name) {
						distances[node.name] = Infinity;
					}
					prev[node.name] = null;
				});

				while (!pq.isEmpty()) {
					let minNode = pq.dequeue();
					animate += 1;
					let currNode = minNode.element;
					// console.log(currNode);

					if (currNode === goal) {
						path.forEach(p => {
							graph.nodes.forEach(gn => {
								if (p.name === gn.name) {
									visitGoal(gn, animate);
								}
							});
						});

						console.log("found");
						break;
					}
					// else {
					// 	console.log("not found");
					// }

					currNode.links.forEach(neighbor => {
						data.forEach(node => {
							if (neighbor.target.name === node.name) {
								let alt = distances[currNode.name] + neighbor.value;
								if (alt < distances[neighbor.target.name]) {
									distances[neighbor.target.name] = alt;
									prev[neighbor.target.name] = currNode;
									pq.enqueue(node, distances[neighbor.target.name]);
									// console.log(node);
									graph.nodes.forEach(gn => {
										if (gn.name === node.name) {
											visitElement(gn, animate);
										}
									});

									if (neighbor.target.name === node.name && source.type1 === neighbor.target.name) {
										path.push({ name: node.name, distance: distances[node.name] });
									} else if (goal.name === node.name && goal.type1 === neighbor.source.name) {
										path.push({ name: node.name, distance: distances[node.name] });
									}

									// console.log(neighbor.target.type2 + " "+ goal.type1)
									// console.log(neighbor.target);
									if (neighbor.target.type2 === goal.type1 && neighbor.target.type1 === source.type1) {
										let alt = distances[node.name];
										if (alt < distances[goalType.name]) {
											distances[goalType.name] = alt;
											prev[goalType.name] = node;
											pq.enqueue(goalType, distances[goalType.name]);
											path.push({
												name: node.name,
												distance: distances[node.name]
											});
											path.push({
												name: goalType.name,
												distance: distances[goalType.name]
											});
										}
									} else if (neighbor.target.name === goal.type1 || neighbor.target.name === goal.type2) {
										let alts = distances[node.name];
										if (alts < distances[goalType.name]) {
											distances[goalType.name] = alts;
											prev[goalType.name] = node;
											pq.enqueue(goalType, distances[goalType.name]);
											path.push({
												name: node.name,
												distance: distances[node.name]
											});
											// path.push({
											// 	name: goalType.name,
											// 	distance: distances[goalType.name]
											// });
										}
									}

									if (node.type1 === goal.type1) {
										path.push({
											name: node.name,
											distance: distances[node.name]
										});
									}
								}
							}
						});
					});

					if (pq.items.length === 0) {
						// console.log('pass')
						link.forEach(item => {
							let alt = distances[item.name];
							if (alt < distances[goalType.name]) {
								distances[goalType.name] = alt;
								prev[goalType.name] = item;
								pq.enqueue(goalType, distances[goalType.name]);
								path.push({ name: item.name, distance: distances[item.name] });

								graph.nodes.forEach(gn => {
									if (gn.name === goalType.name) {
										visitElement(gn, animate);
									}
								});
							}
						});
						path.push({ name: goalType.name, distance: distances[goalType.name] });
						for (let i = 1; i < path.length; i++) {
							if (path[i - 1].distance === path[i].distance) {
								path.splice(i - 2, 1);
							}
						}
					}
				}

				for (let i = 1; i < path.length; i++) {
					if (path[i].name === goal.name) {
						console.log(path[i]);
						path.splice(i + 1);
					}
				}

				console.log(path);
			};

			dijkstra(nodes, start, end);
		});
	};
}
